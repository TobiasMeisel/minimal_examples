#!/bin/bash
#SBATCH -J Benchmark
#SBATCH -p haswell64
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=1
##SBATCH --exclusive
#SBATCH --mail-user=tome821d@tu-dresden.de
#SBATCH --output /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/%j-out.txt
#SBATCH --error /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/%j-err.txt
#SBATCH --time=00:01:00

module purge
module load HDF5/1.12.1
module load CMake/3.21.1


mkdir /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}


srun /home/tome821d/w/minimal/hdf5_minimal/build/rp/hdf5_minimal -f /scratch/ws/0/tome821d-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}/y.h5 -l 1 -c 1000000 -i 0 -s 100
