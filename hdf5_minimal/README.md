
Quick test from this directory:

`ninja -C build -j 2 && mpirun -np 3 build/hdf5_minimal -f y.h5 -l 1 -c 1000000 -i 0 -s 100 && h5dump -p y.h5`

Documentation when calling 
`hdf5_minimal --help`

```

USAGE: 

   ./hdf5_minimal  -c <int> -i <int> -l <int> -s <int> -f <string> [--]
                   [--version] [-h]


Where: 

   -c <int>,  --chunk_size_in_byte <int>
     (required)  Size of chunks in bytes

   -i <int>,  --test_data_inc_per_rank <int>
     (required)  For each mpi process the length of vector is different. It
     increases with specified number per Rank.

   -l <int>,  --test_data_loops <int>
     (required)  Number of test data write loops

   -s <int>,  --test_data_size <int>
     (required)  Test data size

   -f <string>,  --file_name <string>
     (required)  ouput file name

   --,  --ignore_rest
     Ignores the rest of the labeled arguments following this flag.

   --version
     Displays version information and exits.

   -h,  --help
     Displays usage information and exits.


   Creates a HDF-file. Data to be written can be adapted. One MPI-process
   writes s elements. The elements values increase by 1. The initial value
   is l*100+r*10 (r is MPI-RANK). The number of values each MPI process
   writes is+i*p.

   Examples: 

   mpirun -np 3 hdf5_minimal -f y.h5 -l 1 -c 1000000 -i 0 -s 100
   

   hdf5_minimal -f x.h5 -l 2 -c 1000000 -i 0 -s 10

```

Expected output of
`mpirun -np 3 hdf5_minimal -f x.h5 -l 2 -c 1000000 -i 1 -s 3 && h5dump -p x.h5`

```
HDF5 "x.h5" {
GROUP "/" {
   DATASET "1D" {
      DATATYPE  H5T_STD_I32LE
      DATASPACE  SIMPLE { ( 36 ) / ( H5S_UNLIMITED ) }
      STORAGE_LAYOUT {
         CHUNKED ( 250000 )
         SIZE 1000000
      }
      FILTERS {
         NONE
      }
      FILLVALUE {
         FILL_TIME H5D_FILL_TIME_IFSET
         VALUE  H5D_FILL_VALUE_DEFAULT
      }
      ALLOCATION_TIME {
         H5D_ALLOC_TIME_EARLY
      }
      DATA {
      (0): 0, 1, 2, 10, 11, 12, 13, 20, 21, 22, 23, 24, 100, 101, 102, 110,
      (16): 111, 112, 113, 120, 121, 122, 123, 124, 200, 201, 202, 210, 211,
      (29): 212, 213, 220, 221, 222, 223, 224
      }
   }
}
}
```
