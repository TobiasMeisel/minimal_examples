/*
 *
 * In this example, we demonstrate how different ranks can write varying amounts
 * of data to a chunked dataset in parallel. Some ranks don't write any
 * data. The chunk size is chosen arbitrarily and set in CHUNK_SIZE.
 *
 */

#include "hdf5.h"

#include "mpi.h"
#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <numeric>
#include <vector>

#include <tclap/CmdLine.h>


hid_t createHDF5TransferPolicy()
{
    // property list for collective dataset write
    hid_t io_transfer_property = H5Pcreate(H5P_DATASET_XFER);
    H5Pset_dxpl_mpio(io_transfer_property, H5FD_MPIO_COLLECTIVE);
    return io_transfer_property;
}



int main(int argc, char **argv) {


  MPI_Init(&argc, &argv);
  printf("MPI Init");
  int comm_size, comm_rank;
  MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &comm_rank);
  printf("rank: %d, size: %d\n", comm_rank, comm_size);


  TCLAP::CmdLine cmd(
      "Creates a HDF-file. Data to be written can be adapted. One MPI-process "
      "writes s elements. The elements values increase by 1. The initial value "
      "is l*100+r*10 (r is MPI-RANK). The number of values each MPI process "
      "writes is+i*p.\n"
      "Examples: \n"
      "mpirun -np 3 hdf5_minimal -f y.h5 -l 1 -c 1000000 -i 0 -s 100 \n"
      "hdf5_minimal -f x.h5 -l 2 -c 1000000 -i 0 -s 10 \n",
      ' ', "0.9");
  TCLAP::ValueArg<std::string> nameArg("f", "file_name", "ouput file name",
                                       true, "file.h5", "string");
  TCLAP::ValueArg<int> test_data_length("s", "test_data_size", "Test data size",
                                        true, 1, "int");
  TCLAP::ValueArg<int> test_data_loops("l", "test_data_loops",
                                       "Number of test data write loops", true,
                                       1, "int");
  TCLAP::ValueArg<int> test_data_inc(
      "i", "test_data_inc_per_rank",
      "For each mpi process the length of vector is different. It increases "
      "with specified number per Rank.",
      true, 0, "int");
  TCLAP::ValueArg<int> chunk_size_v("c", "chunk_size_in_byte",
                                    "Size of chunks in bytes", true, 1048576,
                                    "int");

  cmd.add(nameArg);
  cmd.add(test_data_length);
  cmd.add(test_data_loops);
  cmd.add(test_data_inc);
  cmd.add(chunk_size_v);
  cmd.parse(argc, argv);
  std::string file_name = nameArg.getValue();
  int data_length = test_data_length.getValue();
  int num_extends = test_data_loops.getValue();
  int inc = test_data_inc.getValue();
  hsize_t chunk_size_byte = chunk_size_v.getValue();

  hid_t data_type = H5T_STD_I32LE;
  hsize_t size_data_set = H5Tget_size(data_type);
  std::cout<<"getsize "<<size_data_set;
  hsize_t chunk_size = chunk_size_byte / size_data_set;

  hid_t fapl = H5Pcreate(H5P_FILE_ACCESS);
  assert(fapl >= 0);
  H5Pset_fapl_mpio(fapl, MPI_COMM_WORLD, MPI_INFO_NULL);
  hid_t file = H5Fcreate(file_name.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, fapl);
  H5Pclose(fapl);
  printf("[%d] File Create %ld\n", comm_rank, file);

  // Let's create some sampl data!
  // MPI ranks have (100 + rank) integers worth of data
  std::vector<int> v(data_length + inc * comm_rank, comm_rank + 1);
  std::iota(std::begin(v), std::end(v), comm_rank * 10);

  // The extent of the dataspace in the file can be computed accordingly.
  hsize_t size =
      data_length * comm_size + inc * comm_size * (comm_size - 1) / 2;

  printf("[%d] Size %d\n", comm_rank, size);

  // Make a dataset!
  // I can't believe that takes 10 lines of code :-(
  hsize_t infty = H5S_UNLIMITED;
  ;
  hid_t fspace = H5Screate_simple(1, &size, &infty);
  hid_t dcpl = H5Pcreate(H5P_DATASET_CREATE);
  printf("[%d] set chunk\n", comm_rank);
  hid_t s = H5Pset_chunk(dcpl, 1, &chunk_size);


  H5Pset_deflate(dcpl, 1);
  hid_t dset = H5Dcreate2(file, "1D", data_type, fspace, H5P_DEFAULT, dcpl,
                         H5P_DEFAULT);

  assert(dset >= 0);
  H5Pclose(dcpl);

  printf("Dataset created\n");
  // Let's get H5Dwriting!
  hsize_t offset{0}, one{1}, blk{v.size()};
  hid_t mspace = H5Screate_simple(1, &blk, NULL);

  H5Sselect_all(mspace);
  // offset = (comm_rank)*data_length +comm_rank*(comm_rank-1)/2;

  offset = data_length * comm_rank + inc * (comm_rank * (comm_rank - 1) / 2);

  printf("offset: %d\n", offset);


  hid_t io_transfer = createHDF5TransferPolicy();
  H5Sselect_hyperslab(fspace, H5S_SELECT_SET, &offset, NULL, &one, &blk) >= 0;

  H5Sselect_all(mspace);
  H5Sselect_hyperslab(fspace, H5S_SELECT_SET, &offset, NULL, &one, &blk);
  printf("[%d] write start\n", comm_rank);

  H5Dwrite(dset, H5T_NATIVE_INT, mspace, fspace, io_transfer, v.data());

  printf("[%d] write done\n", comm_rank);



  // extend
  hsize_t new_size = size;
  for (int k = 0; k < num_extends; ++k) {
    printf("k: %d\n", k);
    std::vector<int> v2(data_length + inc * comm_rank);
    std::iota(std::begin(v2), std::end(v2), comm_rank * 10 + (k + 1) * 100);

    hsize_t dimsext[] = {v2.size()};

    hsize_t offset2[] = {new_size + offset};
    int *dataext = v2.data();
    new_size += size;
    hid_t status = H5Dset_extent(dset, &new_size);
    fspace = H5Dget_space(dset);
    hid_t status2 = H5Sselect_hyperslab(fspace, H5S_SELECT_SET, offset2, NULL,
                                        dimsext, NULL);
    mspace = H5Screate_simple(1, dimsext, NULL);
    status =
        H5Dwrite(dset, H5T_NATIVE_INT, mspace, fspace, io_transfer, dataext);
  }
  // Clean house!
  H5Sclose(mspace);
  H5Dclose(dset);
  H5Sclose(fspace);
  H5Fclose(file);

  printf("Finalize");
  MPI_Finalize();
  printf("Done");
  return EXIT_SUCCESS;
}
